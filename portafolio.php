<?php
include('commons/header.php');
?>

<?php
include('commons/chat-whatsapp.php');
?>

        <!-- Main Menu-->
        <div class="inner-nav navbar-desktop">
          <ul class="clearlist scroll scroll-nav">
              <li class="active"><a href="#pageMember">Inicio</a></li>
              <li class="active"><a href="/jesus-ricano.php">Regresar a perfil</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <section class="no-padding" id="pageMember">
      <div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-xs-12 col-lg-12">
            <div class="portafolio center">
                <br><br>
                <h1 data-effect="fadeInLeft" class="hs-line-1 animate-e alturaBG wow fadeInLeft">Portafolio</h1>
                <div data-effect="fadeInDown" class="hs-line-2 animate-e">Algunos proyectos...</div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="no-padding top50" id="pageMember">
      <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 center">
                <p class="post-title paddingTitlesPortafolio">- Badak -</p>

                <p>(AB Inbev Mystery Shopper)</p>
                <a href="assets/img/portafolio/8/1.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/8/1.png);"></div>
                </a>
                <a href="assets/img/portafolio/8/2.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/8/2.png);"></div>
                </a>
                <a href="assets/img/portafolio/8/3.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/8/3.png);"></div>
                </a>
                <a href="assets/img/portafolio/8/4.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/8/4.png);"></div>
                </a>
                <p>(MC Brokers)</p>
                <a href="assets/img/portafolio/8/5.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/8/5.png);"></div>
                </a>
                <a href="assets/img/portafolio/8/6.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/8/6.png);"></div>
                </a>
                <a href="assets/img/portafolio/8/7.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/8/7.png);"></div>
                </a>
                <a href="assets/img/portafolio/8/8.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/8/8.png);"></div>
                </a>
                <p>(Equipo Electrónico GNP Seguros)</p>
                <a href="assets/img/portafolio/7/1.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/7/1.png);"></div>
                </a>
                <a href="assets/img/portafolio/7/2.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/7/2.png);"></div>
                </a>
                <a href="assets/img/portafolio/7/3.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/7/3.png);"></div>
                </a>
                <p>(RC Profesional GNP Seguros)</p>
                <a href="assets/img/portafolio/7/4.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/7/4.png);"></div>
                </a>
                <a href="assets/img/portafolio/7/5.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/7/5.png);"></div>
                </a>
                <a href="assets/img/portafolio/7/6.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/7/6.png);"></div>
                </a>
            </div>
        </div>
      </div>
    </section>

    <section class="no-padding" id="pageMember">
      <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 center">
                <p class="post-title paddingTitlesPortafolio">- Axity (Walmart) -</p>

                <a href="assets/img/portafolio/6/1.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/6/1.png);"></div>
                </a>
                <a href="assets/img/portafolio/6/2.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/6/2.png);"></div>
                </a>
                <a href="assets/img/portafolio/6/3.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/6/3.png);"></div>
                </a>
                <a href="assets/img/portafolio/6/4.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/6/4.png);"></div>
                </a>
            </div>
        </div>
      </div>
    </section>

    <section class="no-padding" id="pageMember">
      <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 center">
                <p class="post-title paddingTitlesPortafolio">- Alianza -</p>
                <a href="assets/img/portafolio/5/1.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/5/1.png);"></div>
                </a>
                <a href="assets/img/portafolio/5/2.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/5/2.png);"></div>
                </a>
            </div>
        </div>
      </div>
    </section>

    <section class="no-padding" id="pageMember">
      <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 center">
                <p class="post-title paddingTitlesPortafolio">- Anzen (Banamex) -</p>
                <a href="assets/img/portafolio/4/1.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/4/1.png);"></div>
                </a>
                <a href="assets/img/portafolio/4/2.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/4/2.png);"></div>
                </a>
            </div>
        </div>
      </div>
    </section>

    <section class="no-padding" id="pageMember">
      <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 center">
                <p class="post-title paddingTitlesPortafolio">- Neuland -</p>
                <a href="assets/img/portafolio/3/1.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/3/1.png);"></div>
                </a>
                <a href="assets/img/portafolio/3/2.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/3/2.png);"></div>
                </a>
                <a href="assets/img/portafolio/3/3.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/3/3.png);"></div>
                </a>
                <a href="assets/img/portafolio/3/4.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/3/4.png);"></div>
                </a>
                <a href="assets/img/portafolio/3/5.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/3/5.png);"></div>
                </a>
                <a href="assets/img/portafolio/3/6.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/3/6.png);"></div>
                </a>
            </div>
        </div>
      </div>
    </section>

    <section class="no-padding" id="pageMember">
      <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 center">
                <p class="post-title paddingTitlesPortafolio">- Concept Haus -</p>
                <a href="assets/img/portafolio/2/1.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/2/1.png);"></div>
                </a>
                <a href="assets/img/portafolio/2/2.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/2/2.png);"></div>
                </a>
                <a href="assets/img/portafolio/2/3.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/2/3.png);"></div>
                </a>
            </div>
        </div>
      </div>
    </section>

    <section class="no-padding" id="pageMember">
      <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 center">
                <p class="post-title paddingTitlesPortafolio">- Depto. Maestrías UAM Azc -</p>
                <a href="assets/img/portafolio/1/1.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/1/1.png);"></div>
                </a>
                <a href="assets/img/portafolio/1/2.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/1/2.png);"></div>
                </a>
            </div>
        </div>
      </div>
    </section>

    <section class="no-padding" id="pageMember">
      <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 center">
                <p class="post-title paddingTitlesPortafolio">- Lappsii -</p>
                <a href="assets/img/portafolio/0/1.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/0/1.png);"></div>
                </a>
                <a href="assets/img/portafolio/0/2.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/0/2.png);"></div>
                </a>
                <a href="assets/img/portafolio/0/3.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/0/3.png);"></div>
                </a>
                <a href="assets/img/portafolio/0/4.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/0/4.png);"></div>
                </a>
                <a href="assets/img/portafolio/0/5.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/0/5.png);"></div>
                </a>
                <a href="assets/img/portafolio/0/6.png" target="_blank">
                    <div class="box_imgs" style="background-image: url(assets/img/portafolio/0/6.png);"></div>
                </a>
            </div>
        </div>
      </div>
    </section>

<?php
include('commons/footer.php');
?>
