
    <!-- Chat -->
    <div id="box-chat">
        <div class="box-chat-header">
            <div class="bg-perfil-image">
                <div class="bullet-state"></div>
            </div>
            <div class="perfil-state">
                <p>
                    <span>Lappsii</span> <br>
                    Conectado
                </p>
            </div>
        </div>
        <div class="box-chat-message">
            <p>Lappsii</p>
            Hola buen día,<br>
            ¿Como podemos ayudarte?
        </div>
        <div class="box-chat-btn">
            <a href="https://api.whatsapp.com/send?phone=525545334220" target="_blank" class="applyinfo-btn">
            <button><i class="fa fa-whatsapp" aria-hidden="true"></i> Comenzar</button>
            </a>
        </div>
    </div>

    <div id="btn-chat">
        <i class="fa fa-whatsapp" aria-hidden="true"></i>
    </div>
    <!-- end Chat -->