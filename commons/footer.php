      <!-- Page Footer Start-->
      <footer class="page-footer">
        <div class="container text-center">
          <!-- Social links-->
          <!--<div class="social-links">
            <a href="https://www.facebook.com/lappsii/" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="https://www.youtube.com/channel/UC93dKBMwXWtlXIKPKN5BQRQ" title="You tube" target="_blank"><i class="fa fa-youtube"></i></a>
            <a href="mailto:admin@lappsii.com" title="Email"><i class="fa fa-envelope"></i></a>
          </div>-->
          <div class="copy-right mt-50">
            <p id="copyRight"></p>
            <script>document.getElementById("copyRight").innerHTML = `© Lappsii ${new Date().getFullYear()}`</script>
            <p>Todos los derechos reservados</p>
          </div>
        </div>
      </footer>
      <!-- Page footer end -->
    </div>

    <!--JS scripts-->
    <script src="assets/js/jquery-2.1.3.min.js"></script>
    <script src="assets/js/smoothscroll.min.js"></script>
    <script src="assets/js/plagins.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5h017Dor9cckkLwDxlrRZ-lV6Kr_x060" type="text/javascript"></script>
    <script src="assets/js/gmap3.min.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/main.min.js"></script>
    <script src="assets/js/operaciones.js"></script>

  </body>
</html>
