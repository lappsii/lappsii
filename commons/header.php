<!DOCTYPE html>
<html ng-app="root">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="Lappsii es una empresa innovadora y comprometida altamente capacitada con más de 7 años de experiencia para brindar un servicio de calidad a nuestros clientes. Estamos conscientes de que este nuevo paradigma del desarrollo web es una potente herramienta para que tu empresa o idea tenga éxito.">
    <meta name="author" content="Lappsii.com">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link rel="shortcut icon" href="assets/img/icons/faviconBlue.png">
    <title>Lappsii</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500" rel="stylesheet">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/chat-whatsapp.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/owl.carousel.min.css" rel="stylesheet">
    <link href="assets/css/vertical.min.css" rel="stylesheet">
    <!-- sweet alert -->
    <link rel="stylesheet" type="text/css" href="assets/sweetalert/dist/sweetalert.css">
    <script src="assets/sweetalert/dist/sweetalert.min.js"></script>
    <!-- WOW -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script>
      new WOW().init();
    </script>
  </head>

    <body class="appear-animate">

        <progress value="0" id="progressBar">
          <div class="progress-container">
            <span class="progress-bar"></span>
          </div>
        </progress>

        <div class="page-loader">
          <div class="loader">Loading</div><span class="loader-text alt-font">Lappsii</span>
        </div>
        <div id="top"></div>
        <!-- Navigation panel-->
        <nav class="main-nav white transparent stick-fixed">
          <div class="full-wrapper relative clearfix">
            <div class="nav-logo-wrap"><a href="/" class="logo">La<span class="higl">pp</span>sii</a></div>
            <div class="navbar-mobile"><i class="fa fa-bars"></i></div>
