<?php
include('commons/header.php');
?>

<?php
include('commons/chat-whatsapp.php');
?>

        <!-- Main Menu-->
        <div class="inner-nav navbar-desktop">
          <ul class="clearlist scroll scroll-nav">
            <li class="active"><a href="/">Página principal</a></li>
            <li class="active"><a href="#pageMember">Inicio</a></li>
            <li class="active"><a href="#about">Acerca de mí</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <section class="no-padding" id="pageMember">
      <div class="cantainer-fluid">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-xs-12 col-lg-12">
            <div class="bg-members center">
                <br>
                <h1 data-effect="fadeInLeft" class="hs-line-1 animate-e alturaBG wow fadeInLeft">Jesús Ricaño</h1>
                <div data-effect="fadeInDown" class="hs-line-2 animate-e">Un desarrollador web creativo</div>
                <div class="social-links">
                  <!-- <a href="https://www.facebook.com/jesusrq92" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a> -->
                  <a href="mailto:jesusrq92@gmail.com" title="Email"><i class="fa fa-envelope"></i></a>
                </div>
                <img src="assets/img/team/p1.jpg" class="img-circle perfilMembers">
                <div data-effect="fadeInUp" class="mt-30 animate-e wow fadeInUp">
                  <a href="#page" class="scroll btn btn-coffee btn-border-w btn-round btn-medium hidden-xs">Empezar</a><span>&nbsp; &nbsp;</span>
                  <a href="assets/file/CV_ACTUAL.pdf" target="_blank" class="scroll btn btn-coffee btn-border-w btn-round btn-medium">Ver CV</a>
                </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div id="page" class="page">
      <section id="about">
        <div class="container">
          <div class="row mt-40 mt-xs-20">
            <div class="col-md-12 text-center">
              <div class="contenedor">
                <h3>Acerca de <b class="azulTitles">mí..</b><span id="m_aboutme">&#160;</span></h3>
              </div>
            </div>
          </div>
          <div class="row mt-40 mt-xs-40">
            <div class="col-sm-12">
              <blockquote class="about-quote">
                <p class="text-justify center-block width70">
                  Ingeniero en Computación apasionado a la tecnología y desarrollo web, egresado de la UAM Azcapotzalco CDMX. <br><br>

                  Cuento con alta experiencia en el desarrollo Frontend. Así mismo he desarrollado grandes proyectos de calidad mundial. <br><br>

                  Me considero una persona humilde, amable, pacífica, trabajadora y de retos pues desde joven he mostrado mucha responsabilidad y lealtad en mis objetivos.<br><br>

                  Una de mis más grandes aspiraciones es desarrollarme como empresa creciendo profesionalmente.<br><br>

                  Disfruto lo que hago, me encanta aprender, soy muy constante, puntual y trabajo hasta terminar siempre lo prometido. <br><br>

                  Diviértete en lo que haces y jamás trabajarás. <br><br>

                  <a href="/portafolio.php" class="link_boldBlue">Ver mi portafolio...</a>
                </p>
              </blockquote>
            </div>
          </div>
        </div>
      </section>

      <style>
        @media screen and (max-width: 1024px) {
          {width: 90% !important;}
          .CSS3{width: 90% !important;}
          .JS{width: 75% !important;}
          .JQuery{width: 75% !important;}
          .Bootstrap{width: 85% !important;}
          .PHP{width: 50% !important;}
          .MySQL{width: 85% !important;}
          .Git{width: 85% !important;}
          .Emmet{width: 80% !important;}
          .SASS{width: 70% !important;}
        }
        @media screen and (max-width: 330px) {
          .btn-coffee.btn-border-w {
            margin-top: -10px;
          }
        }
      </style>

      <section id="team">
        <div class="fullwidth-testimotal-slider bg-dark-80 pt-100 pb-100">
         <div>
            <div class="container">
              <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                  <div class="section-icon"><span class="icon-quote"></span></div>
                  <h3 class="alt-font">Certificado en Evaluador de experiencia de usuario (UX)</h3>
                  <blockquote class="testimonial">
                    <p>- Certificación por Fundación Carlos Slim | 2024-10-08 <a href="assets/file/doc8_jesus_ricano.pdf" target="blank"><sub>Ver</sub></a> </p>
                  </blockquote>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div class="container">
              <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                  <div class="section-icon"><span class="icon-quote"></span></div>
                  <h3 class="alt-font">Certificado en Desarrollador de JavaScript (React) </h3>
                  <blockquote class="testimonial">
                    <p>- Certificación por Fundación Carlos Slim | 2024-10-03 <a href="assets/file/doc7_jesus_ricano.pdf" target="blank"><sub>Ver</sub></a> </p>
                  </blockquote>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div class="container">
              <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                  <div class="section-icon"><span class="icon-quote"></span></div>
                  <h3 class="alt-font">Certificado en Reparación de laptops </h3>
                  <blockquote class="testimonial">
                    <p>- Certificación por Fundación Carlos Slim | 2024-09-14 <a href="assets/file/doc6_jesus_ricano.pdf" target="blank"><sub>Ver</sub></a> </p>
                  </blockquote>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div class="container">
              <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                  <div class="section-icon"><span class="icon-quote"></span></div>
                  <h3 class="alt-font">SCRUM Foundation <br>Professional Certificate SFPC<sup>TM</sup></h3>
                  <blockquote class="testimonial">
                    <p>- Certificación por CertiProf Professional Knowledge | 2022-11-24 <a href="assets/file/doc5_jesus_ricano.pdf" target="blank"><sub>Ver</sub></a> </p>
                  </blockquote>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div class="container">
              <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                  <div class="section-icon"><span class="icon-quote"></span></div>
                  <h3 class="alt-font">Diplomado en Gestión de Proyectos </h3>
                  <blockquote class="testimonial">
                    <p>- Certificación por Edutin Academy | 2022-11-23 <a href="assets/file/doc4_jesus_ricano.pdf" target="blank"><sub>Ver</sub></a> </p>
                  </blockquote>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div class="container">
              <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                  <div class="section-icon"><span class="icon-quote"></span></div>
                  <h3 class="alt-font">Diseño de Base de Datos con MYSQL</h3>
                  <blockquote class="testimonial">
                    <p>- Certificación por IT Open Knowledg | 2016-04-29 <a href="assets/file/doc3_jesus_ricano.pdf" target="blank"><sub>Ver</sub></a> </p>
                  </blockquote>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div class="container">
              <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                  <div class="section-icon"><span class="icon-quote"></span></div>
                  <h3 class="alt-font">Fundamentos del Diseño Web</h3>
                  <blockquote class="testimonial">
                    <p>- Certificación por IT Open Knowledge | 2016-04-21 <a href="assets/file/doc2_jesus_ricano.pdf" target="blank"><sub>Ver</sub></a> </p>
                  </blockquote>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div class="container">
              <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                  <div class="section-icon"><span class="icon-quote"></span></div>
                  <h3 class="alt-font">JavaScript con JQuery</h3>
                  <blockquote class="testimonial">
                    <p>- Certificación por IT Open Knowledge | 2015-10-17 <a href="assets/file/doc1_jesus_ricano.pdf" target="blank"><sub>Ver</sub></a> </p>
                  </blockquote>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <hr>

<?php
include('commons/footer.php');
?>
