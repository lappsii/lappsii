<?php 
include('commons/header.php');
?>
	<style>
		body{
			background-image: url('assets/img/full-width/bg-contact.jpg');
			background-position: center center;
			background-repeat: no-repeat;
			background-size: cover;
		}
	</style>

        <!-- Main Menu-->
        <div class="inner-nav navbar-desktop">
          <ul class="clearlist scroll scroll-nav">
            <li class="active"><a href="/">Página principal</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <section>
    	<div class="row">
    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 error404">
    			<h1>¡LO SENTIMOS!</h1>
    			<hr>
    			<div>
    				<h2>Error <span>404</span> :(</h2>
					<p>
						La página que intentas solicitar no está en el servidor. <br>
					</p>
    			</div>
    		</div>
    	</div>
    </section>

<?php
include('commons/footer.php');
?>