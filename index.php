<?php
include('commons/header.php');
?>

<?php
include('commons/chat-whatsapp.php');
?>

        <!-- Main Menu-->
        <div class="inner-nav navbar-desktop">
          <ul class="clearlist scroll scroll-nav">
            <li class="active"><a href="#hero">Inicio</a></li>
            <li><a href="#about">Nosotros</a></li>
            <li><a href="#service">Servicios</a></li>
            <li><a href="#contacts">Contactar</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 anchoCol" id="video_pattern">
            <video autoplay muted loop id="video_background" preload="auto">
              <source src="assets/video/lappsii_video.mp4" type="video/mp4" />
            </video>
            <!--<div id="bg_main"></div>-->

            <!--Start Slider-->
            <div class="fullwidth-slider owl-high-pagination bg-dark-50">
              <div class="full-screen bg-img">
                <div class="hero-content">
                  <div class="hero-content-inner">
                    <div data-effect="fadeInDown" class="hs-line-2 animate-e">Web y diseño a la medida</div>
                    <h1 data-effect="fadeInLeft" class="hs-line-1 animate-e">Sé parte de la evolución</h1>
                    <div data-effect="fadeInDown" class="hs-line-2 animate-e">Lappsii Creative Studio</div>
                    <div data-effect="fadeInUp" class="mt-30 animate-e">
                      <a href="#page" class="scroll btn btn-coffee btn-border-w btn-round btn-medium">
                        Empezar</a>
                      <span>&nbsp; &nbsp;</span>
                    </div>
                  </div>
                </div>
                <div class="contentScroll"><div class='icon-scroll'></div></div>
              </div>
              <div class="full-screen bg-img">
                <div class="hero-content">
                  <div class="hero-content-inner">
                    <div data-effect="fadeInDown" class="hs-line-2 animate-e">Diseño responsivo a la medida</div>
                    <h1 data-effect="fadeInLeft" class="hs-line-1 animate-e">Tu sitio web en móviles</h1>
                    <div data-effect="fadeInDown" class="hs-line-2 animate-e">Lappsii Creative Studio</div>
                  </div>
                </div>
                <div class="contentScroll"><div class='icon-scroll'></div></div>
              </div>
              <div class="full-screen bg-img">
                <div class="hero-content">
                  <div class="hero-content-inner">
                    <div data-effect="fadeInDown" class="hs-line-2 animate-e">Ponte en contacto</div>
                    <h1 data-effect="fadeInLeft" class="hs-line-1 animate-e">Soporte 24/7</h1>
                    <div data-effect="fadeInDown" class="hs-line-2 animate-e">Lappsii Creative Studio</div>
                    <div data-effect="fadeInUp" class="mt-30 animate-e">
                      <a href="#contacts" class="scroll btn btn-coffee btn-border-w btn-round btn-medium">
                        Contactar
                      </a>
                    </div>
                  </div>
                </div>
                <div class="contentScroll"><div class='icon-scroll'></div></div>
              </div>
            </div>
            <!-- end slider -->
          </div>
        </div>
      </div>
    </section>

    <div id="page" class="page page-full-view">
      <!--Start section about-->
      <section id="about">
        <div class="container">
          <div class="row mt-40 mt-xs-20">
            <div class="col-md-12 text-center">
              <div class="contenedor">
                <h3>Acerca de <b class="azulTitles">Nosotros..</b><span id="m_nosotros">&#160;</span></h3>
              </div>
            </div>
          </div>
          <div class="row mt-60 mt-xs-30">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 center">
                <div class="devices" data-wow-delay="0.1s" data-wow-duration="1s" class="wow fadeInLeft">
                <img id="desktop" src="assets/img/devices/desktop.png" alt="desktop" data-wow-delay="0.1s" data-wow-duration="2s" class="wow fadeInLeft">
                </div>
                <div class="devices" data-wow-delay="0.1s" data-wow-duration="1s" class="wow fadeInRight">
                <img id="phone" src="assets/img/devices/phone.png" alt="phone" data-wow-duration="2s" class="wow fadeInRight">
                </div>
            </div>
          </div>
          <div class="row mt-60 mt-xs-30">
            <div class="col-md-8 col-md-offset-2 text-center">
              <blockquote data-wow-delay="0.1s" data-wow-duration="1s" class="about-quote wow fadeInUp">
                <p>Lappsii es una empresa innovadora y comprometida altamente capacitada con más de 6 años de experiencia para brindar un servicio de calidad a nuestros clientes. </p>
                <p>La web se ha convertido en un elemento central de innovación en empresas de todos los sectores, así que si nos queremos mantener actualizados, necesitamos hacerlo parte de nuestro arsenal.</p>
                <footer class="about-quote-author">
                  <a href="jesus-ricano.php">Jesús Ricaño</a>, Desarrollador Web | Fundador
                </footer>
              </blockquote>
            </div>
          </div>
        </div>
        <div id="facts" class="small-section bg-light mt-80 mt-xs-80 parallax1">
          <div class="container">
            <div class="small-section pt-0 pb-0">
              <div class="container">
                <div class="row">
                  <div class="col-md-10 col-md-offset-1">
                    <div class="small-carousel owl-carousel">
                      <div class="logo-item"><img src="assets/img/clients/html5.png" width="80" height="80" alt=""></div>
                      <div class="logo-item"><img src="assets/img/clients/css3.png" width="80" height="80" alt=""></div>
                      <div class="logo-item"><img src="assets/img/clients/js.png" width="80" height="80" alt=""></div>
                      <div class="logo-item"><img src="assets/img/clients/jquery.png" width="80" height="80" alt=""></div>
                      <div class="logo-item"><img src="assets/img/clients/bootstrap.png" width="80" height="80" alt=""></div>
                      <div class="logo-item"><img src="assets/img/clients/php.png" width="110" height="80" alt=""></div>
                      <div class="logo-item"><img src="assets/img/clients/mysql.png" width="80" height="80" alt=""></div>
                      <div class="logo-item"><img src="assets/img/clients/git.png" width="80" height="80" alt=""></div>
                      <div class="logo-item"><img src="assets/img/clients/filezilla.png" width="80" height="80" alt=""></div>
                      <div class="logo-item"><img src="assets/img/clients/emmet.png" width="80" height="80" alt=""></div>
                      <div class="logo-item"><img src="assets/img/clients/sass.png" width="80" height="80" alt=""></div>
                      <div class="logo-item"><img src="assets/img/clients/visual.png" width="80" height="80" alt=""></div>
                      <div class="logo-item"><img src="assets/img/clients/brackets.png" width="80" height="80" alt=""></div>
                      <div class="logo-item"><img src="assets/img/clients/photoshop.png" width="80" height="80" alt=""></div>
                      <div class="logo-item"><img src="assets/img/clients/react.png" width="80" height="80" alt=""></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--Start section Service-->
      <section id="service" class="pb-40">
        <div class="container">
          <div class="row mt-40 mt-xs-20">
            <div class="col-md-12 text-center">
              <div class="contenedor">
                <h3>Nuestros <b class="azulTitles">Servicios..</b><span id="m_servicios">&#160;</span></h3>
              </div>
            </div>
          </div>
          <div data-wow-delay="0.1s" data-wow-duration="2s" class="row mt-60 mt-xs-30 wow fadeIn">
            <div class="col-sm-4 text-center">
              <div class="service-item"><i class="fa fa-laptop circleServices"></i>
                <h5 class="alt-font"><span class="boldLetters">Dis</span>eño y Desarrolo</h5>
                <div class="service-desc">
                  <p> Diseñamos y desarrollamos sitios web profesionales, limpios y de fácil navegación utilizando las mejores herramientas para optimización de su sitio.</p>
                </div>
              </div>
            </div>
            <div class="col-sm-4 text-center">
              <div class="service-item"><i class="fa fa-mobile circleServices"></i>
                <h5 class="alt-font"><span class="boldLetters">Dis</span>eño Responsivo</h5>
                <div class="service-desc">
                  <p> Implementamos diseño responsivo para que tu sitio web sea adaptable a dispositivos móviles y compatible con cualquier navegador.</p>
                </div>
              </div>
            </div>
            <div class="col-sm-4 text-center">
              <div class="service-item"><i class="fa fa-globe circleServices"></i>
                <h5 class="alt-font"><span class="boldLetters">Reg</span>istro de Dominio</h5>
                <div class="service-desc">
                  <p> Te ofrecemos un dominio .com completamente gratis. Si no sabes que nombre de dominio usar, nosotros te ayudamos.
                  <br>Ej. nombrededominio.com</p>
                </div>
              </div>
            </div>
            <div class="col-sm-4 text-center">
              <div class="service-item"><i class="fa fa-building-o circleServices"></i>
                <h5 class="alt-font"><span class="boldLetters">Hos</span>pedaje</h5>
                <div class="service-desc">
                  <p> Te ofrecemos alojamiento web en los mejores servidores de E.U.A para que tu sitio este disponible las 24 horas del día.</p>
                </div>
              </div>
            </div>
            <div class="col-sm-4 text-center">
              <div class="service-item"><i class="fa fa-line-chart circleServices"></i>
                <h5 class="alt-font"><span class="boldLetters">Opt</span>imización de código</h5>
                <div class="service-desc">
                  <p> Garantizamos el rendimiento y velocidad de tu sitio con código completamente limpio, aprovechando al máximo todos los recursos que la web nos ofrece.</p>
                </div>
              </div>
            </div>
            <div class="col-sm-4 text-center">
              <div class="service-item"><i class="fa fa-phone circleServices"></i>
                <h5 class="alt-font"><span class="boldLetters">Sop</span>orte</h5>
                <div class="service-desc">
                  <p> Porque para nosotros es importante la atención al cliente, te brindamos soporte las 24 horas del día los siete días de la semana.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="quotation">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 text-center font-quotation">
              <strong>¿TE GUSTA LA ERA DIGITAL? </strong><br>CONOCE IDEAS DIGITALES YA
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
              <div data-effect="fadeInUp" class="animate-e">
                <a href="/ideasdigitales" target="_blank" class="scroll">
                  <button class="center-block btn-quotation">
                    ENTRA AQUÍ
                  </button>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!--Contacts section-->
      <section id="contacts" class="pt-0">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-center mt-60 mt-xs-30">
              <div class="contenedor">
                <h3>Ponte en <b class="azulTitles">Contacto..</b><span id="m_contactar">&#160;</span></h3>
              </div>
            </div>
          </div>
          <div class="row mt-20">
            <div class="col-md-8 col-md-offset-2 text-center">
              <p> Para Lappsii es un placer atenderte. Por eso te ponemos a disposición diferentes medios de contacto para atenderte en el momento que más desees.</p>
            </div>
          </div>
        </div>
        <div class="container-fluid">
          <div class="row bg-contact">
            <div class="col-md-8 col-md-offset-2 mt-40">
              <form class="form form-contact" onsubmit="return false;">
                <div class="col-md-12">
                  <i class="fa fa-user" aria-hidden="true"></i>
                  <input id="name" name="name" type="text" placeholder="Nombre*" class="form-control input-md" required>
                </div>
                <div class="col-md-6">
                  <i class="fa fa-envelope fa-envelope-form" aria-hidden="true"></i>
                  <input id="email" name="email" type="email" placeholder="Email*" class="form-control input-md" required>
                </div>
                <div class="col-md-6">
                  <i class="fa fa-building" aria-hidden="true"></i>
                  <input id="subject" name="subject" type="text" placeholder="Asunto*" class="form-control input-md" required>
                </div>
                <div class="col-md-12">
                  <i class="fa fa-comment" aria-hidden="true"></i>
                  <textarea id="message" name="message" placeholder="Mensaje*" class="form-control input-md" required></textarea>
                </div>
                <div class="col-md-6">
                  <button id="btn-reset" type="reset" class="btn btn-coffee btn-medium btn-100">Limpiar</button>
                </div>
                <div class="col-md-6">
                  <button id="whatsContact" class="btn btn-coffee btn-medium btn-100">Enviar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
      <!--Contacts section end -->

<?php
include('commons/footer.php');
?>
